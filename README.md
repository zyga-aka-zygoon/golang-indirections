# Welcome

This project is designed to host cross-distribution dicussions around golang
packaging. Feel free to report new issues for discussions and the wiki for
documentation

issues: https://gitlab.com/zygoon/golang-indirections/issues
wiki: https://gitlab.com/zygoon/golang-indirections/wikis/home

# FAQ

## Q: Is there any code here?
Not yet. Perhaps we will agree on something and then the project can host
shared implementation of packaging wrappers that more than one distribution
chooses to adopt.

